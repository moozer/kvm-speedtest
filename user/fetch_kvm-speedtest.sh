#!/usr/bin/env bash

BOX_FILENAME="kvm-speedtest.box"

echo "Fetching latest kvm-speedtest from vagrantcloud"
LATEST_URL=$(curl https://app.vagrantup.com/api/v1/user/moozer 2> /dev/null \
              | json_pp \
              | grep "download_url" \
              | grep "/kvm-speedtest/" | grep "libvirt" \
              | cut -d'"' -f4)

wget -N $LATEST_URL -O $BOX_FILENAME

echo adding box to vagrant
vagrant box add $BOX_FILENAME --name "moozer/kvm-speedtest"

echo "creating vagrant file"
wget -N https://gitlab.com/moozer/kvm-speedtest/raw/master/user/Vagrantfile

echo "all set"
echo "type 'vagrant up' to boot the VM followed by 'vagrant ssh -c './do_benchmark.sh 1 3' to do the actual benchmarking."
