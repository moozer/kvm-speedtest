#!/usr/bin/env bash

BASENW=130
BOX_FILENAME='kvm-speedtest.box'
BOX_NAME="moozer/kvm-speedtest"
MAX_MEMORY=3096
MEMORY_PER_VM=400
BM_SCRIPT="./benchmark.sh"

if [ "x" == "x$2" ]; then
  echo "usage: $0 <curcount> <maxcount>"
  exit 2
fi

CURCOUNT=$1
MAXCOUNT=$2
NESTCOUNT=$(($CURCOUNT+1))

echo "spinning up nested VM - #$NESTCOUNT"
mkdir -p kvm-test
cd kvm-test

echo "using subnet $(($BASENW+$NESTCOUNT))"

echo "generating vagrant file"
../gen_vagrantfile.sh $(($BASENW+$NESTCOUNT)) $(($MAX_MEMORY - $CURCOUNT * $MEMORY_PER_VM)) > Vagrantfile

echo "copying $BOX_FILENAME from host"

if [ -f /vagrant/$BOX_FILENAME ]; then
  cp /vagrant/$BOX_FILENAME .
else
  echo "Cannot find box file /vagrant/$BOX_FILENAME"
  echo "are shared folders from local to /vagrant enabled?"
  exit 1
fi

echo "adding box"
vagrant box add $BOX_FILENAME --name $BOX_NAME > vagrant_log_${NESTCOUNT}.log

echo "starting nested VM"
echo "vagrant up default" >> vagrant_log_${NESTCOUNT}.log
vagrant up  >> vagrant_log_${NESTCOUNT}.log

echo "vagrant ssh -c \"$0 $NESTCOUNT $MAXCOUNT\"" >> vagrant_log_${NESTCOUNT}.log
vagrant ssh -c "$BM_SCRIPT $NESTCOUNT $MAXCOUNT"

echo "Stopping vagrant box #${NESTCOUNT}"
echo "vagrant halt" >> vagrant_log_${NESTCOUNT}.log
vagrant halt >> vagrant_log_${NESTCOUNT}.log
