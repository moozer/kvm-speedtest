#!/usr/bin/env bash

BMFILE="benchmark.txt"
HDDDEV="/dev/vda"

echo "------"
echo ""
echo "  Doing benchmarking  "

echo ""
echo "------"
echo "checking vda disk speed"
sudo hdparm -Tt $HDDDEV | tee hdparm.tmp
sed -n 's/.* \(.*\) MB\/sec/\1/p' hdparm.tmp > $BMFILE

echo ""
echo "------"
echo "checking cpu"
{ time dd if=/dev/urandom bs=1M count=1k | sha256sum ;}  2> dd-speed.tmp ; cat dd-speed.tmp
sed -n 's/.* \(.*\) MB\/s/\1/p' dd-speed.tmp >> $BMFILE
sed -n 's/real\t\(.*\)/\1/p' dd-speed.tmp >> $BMFILE

echo ""
echo "------"
echo "checking network not implemented."
echo "feel free to come up with test suggestions"
echo "indications of speed are in the download of the vagrant box"

echo ""
echo "------"

cat $BMFILE | tr "\n" "\t" > $BMFILE
echo "" >> $BMFILE

echo " benchmarks saved in $BMFILE"
echo "------"
