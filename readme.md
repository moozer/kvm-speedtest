KVM speedtest
====================

The goal is to have a convenient way of speed testing a virtual setup.

I have issues related to speeds of nested virtual devices, and this project aim to detect and notify about misconfigurations of the host and hypervisor.

Inspiration came from [here](https://www.redhat.com/en/blog/inception-how-usable-are-nested-kvm-guests)

Functionality
--------------------

1. Download box from vagrantcloud to main host (L0)
1. The user initialises and starts the vagrant box `moozer/kvm-speedtest` on L0
2. The user starts the benchmarking script on the vagrant box (L1)
3. The program do benchmarks on L1
3. The base box is copied from the host L0 over a shared folder to L1
4. A new vagrant box is initialised inside L1 and started up automatically
5. The benchmark script is then run on the nested vagrant box (L2, vagrant-within-vagrant)
5. The base box is copied from the host L1 over a shared folder to L2
4. A new vagrant box is initialised inside L2 and started up automatically
5. The benchmark script is then run on the double-nested vagrant box (L3, vagrant-within-vagrant-within-vagrant)

and so on, until the specified nesting level is achieved. In technical terms, we are doing recursion with the vagrant boxes.

Usage
---------

I have a script the makes it easy. To download do

```
wget https://gitlab.com/moozer/kvm-speedtest/raw/master/user/fetch_kvm-speedtest.sh
```

Basic functionality is
1. get link to latest version of `moozer/kvm-speedtest`
2. download it
3. add box to host
4. create `Vagrantfile`

To do the actual benchmarking, you need to 
```
vagrant up
vagrant ssh -c './do_benchmark.sh 1 3'
```

This will output the result of running the tests on 3 nesting levels, starting from 1.

Notice that vagrant defaults to using virtualbox, and the current version of KVM-speedtest only supports qemu/kvm through libvirt.


Errors
-----------

### `vagrant up`

If you get error like `cannot set up guest memory 'pc.ram': cannot allocate memory`, you need to ensure that the initial vagrant box has sufficient memory. At least 2GB is expected.

Add something like this to the `Vagrantfile`
```
  config.vm.provider :libvirt do |libvirt|
    libvirt.memory = 2048
  end
```

### preflight check

```
* bad : nested KVM *not* enabled in the module
```

It means that the kvm_intel module has not the nesting feature enabled. It is seen in `/sys/module/kvm_intel/parameters/nested`.

It is set at boot time by adding a line like `options kvm_intel nested=1` in `/etc/modprobe.d/kvm_intel_nested.conf`.


```
* bad : CPU virtualization extensions *not* available
```

The CPU inside the vagrant does have cpu virtualization enabled. For intel cpus it is the "vmx" flags.

On the host machine, try running the [kvm sanity check script](https://raw.githubusercontent.com/moozer/ansible-server-kvm/master/files/kvm_sanity_check.sh) which is run in the beginning of the benchmarking test.



