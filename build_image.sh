#!/usr/bin/env bash

tmp_dir="packer_tmp"
packer_cache_dir="packer_cache"
build_template="base.json"
packer_log_file="packer.log"
build_vars="variables.json"

if [ ! -d "${packer_cache_dir}" ]; then
    mkdir -p "${packer_cache_dir}"
fi

if [ ! -d "${tmp_dir}" ]; then
    mkdir -p "${tmp_dir}"
fi

PACKER_LOG=1 \
TMPDIR="${tmp_dir}" \
CHECKPOINT_DISABLE=1 \
PACKER_CACHE_DIR="${packer_cache_dir}" \
packer build -only=qemu -var-file=${build_vars} ${build_template} \
	| tee ${packer_log_file}

PACKER_RESULT=$?

exit $PACKER_RESULT
