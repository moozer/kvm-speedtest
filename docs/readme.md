Vagrantcloud
------------------

The vagrantcloud postprocessor started giving `422 Unprocessable...`. This is most likely a ratelimiting issue. see [here](https://www.vagrantup.com/docs/vagrant-cloud/api.html#client-errors)

That makes vagrant cloud unsuited for use while developping.

To reenable add this in `base.json`:
```
"post-processors": [
  [
    {
      "type": "vagrant",
      "keep_input_artifact": true,
      "vagrantfile_template": "scripts/cache/Vagrantfile",
      "output": "kvm-speedtest-{{user `version`}}.{{.Provider}}.box"
    },
    {
      "type": "vagrant-cloud",
      "box_tag": "moozer/kvm-speedtest",
      "version": "{{user `version`}}",
      "version_description":  "{{user `version_description`}}"
    }
  ]
]
```

For testing and deployment of non-vagrant boxes, use `manifest` and `shell-local` post-processors. See [here](https://www.packer.io/docs/post-processors/shell-local.html#interacting-with-build-artifacts)
