#!/usr/bin/env bash

# using the vagrantup api:
# https://www.vagrantup.com/docs/vagrant-cloud/api.html#organizations

# find the download url for the latest stretch64 vagrant box
LATEST_URL=$(curl https://app.vagrantup.com/api/v1/user/debian 2> /dev/null \
              | json_pp \
              | grep "download_url" \
              | grep "/stretch64/" | grep "libvirt" \
              | cut -d'"' -f4)

echo "Base download URL"
echo "  $LATEST_URL"

mkdir -p cache
cd cache
echo ""
echo "Downloading"
wget -N $LATEST_URL

if [ -f libvirt.box.md5 ]; then
    if md5sum -c libvirt.box.md5; then
      echo "md5sums match. Assuming no change"
      exit 0
    fi

    echo "md5sum mismatch. Assuming file changes"
    rm box.img Vagrantfile metadata.json libvirt.box.md5
fi

echo "caculating md5 and extracting"
md5sum libvirt.box > libvirt.box.md5
tar xf libvirt.box
